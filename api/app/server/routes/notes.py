from fastapi import APIRouter

note_router = APIRouter()

notes = ["Clear task", "Seminar", "Learn", "Sleep"]


@note_router.get('/', response_description="GET ALL NOTE")
async def read_notes():
    return notes
