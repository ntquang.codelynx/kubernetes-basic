from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from server.routes.notes import note_router as NoteRouter

app = FastAPI()

origins = [
    "http://localhost:3001",
    "http://localhost"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(NoteRouter, tags=["Notes"], prefix="/notes")


@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Server is running"}
