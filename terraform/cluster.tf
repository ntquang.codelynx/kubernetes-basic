# Local registry 
provider "docker" {
  host = "unix:///var/run/docker.sock"
}

resource "docker_container" "registry" {
  image = "registry:2"
  name  = "kind-registry"
  start = true
  ports {
    internal = 5000
    external = 5000
  }
}

# Cluster
provider "kind" {
}

provider "kubernetes" {
  config_path = pathexpand(var.cluster_config_path)
}

resource "kind_cluster" "default" {
  name            = var.cluster_name
  kubeconfig_path = pathexpand(var.cluster_config_path)
  node_image      = "kindest/node:v1.19.0"
  wait_for_ready  = true

  kind_config {
    kind        = "Cluster"
    api_version = "kind.x-k8s.io/v1alpha4"

    containerd_config_patches = [
      <<-TOML
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."localhost:5000"]
          endpoint = ["http://kind-registry:5000"]
      TOML
    ]

    node {
      role                   = "control-plane"
      kubeadm_config_patches = ["kind: InitConfiguration\nnodeRegistration:\n  kubeletExtraArgs:\n    node-labels: \"ingress-ready=true\"\n"]

      extra_port_mappings {
        container_port = 80
        host_port      = 80
      }

      extra_port_mappings {
        container_port = 443
        host_port      = 443
      }

      # For NodePort 
      extra_port_mappings {
        container_port = 30950
        host_port      = 3000
      }

      # For NodePort 
      extra_port_mappings {
        container_port = 30951
        host_port      = 3001
      }
    }

    node {
      role = "worker"
    }

    node {
      role = "worker"
    }
  }
}

# Connect Kind netword and local registry network 
resource "null_resource" "connect_registry" {
  triggers = {
    key = uuid()
  }

  provisioner "local-exec" {
    command = <<EOF
      docker network connect kind kind-registry || true
      kubectl apply -f local_registry_configmap.yaml
    EOF
  }

  depends_on = [docker_container.registry, kind_cluster.default]
}
